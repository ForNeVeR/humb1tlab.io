+++
title = "Nikita Puzankov"
description = "Professional Software Engineer"
date = 2019-05-07
[taxonomies]
tags = ["personal", "me", "dev", "cv"]
categories = ["development", "personal"]

+++

## This page about me as a professional developer and can be used as a CV.

Since 2013 I'm a Java Developer with wide experience and great soft skills.
Main expertise is a cloud-native java development using `Spring`. More information about current position and carrer on [LinkedIn](https://www.linkedin.com/in/humb1t/).

I'm also interested and very enthusiastic about `Rust`, see my talks in Russian:
- [Rust vs Java](https://www.youtube.com/watch?v=yfQhMxZ0JYc)
- [Rust or There and Back again](https://www.youtube.com/watch?v=90mxemR3A3I)

Some contributions on [GitHub](https://github.com/humb1t) and [GitLab](https://gitlab.com/humb1t).

Feel free to find me via @humb1t in Telegram or Twitter.
